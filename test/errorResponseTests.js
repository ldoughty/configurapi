const assert = require('chai').assert;
const ErrorResponse = require('../src/errorResponse');
const equal = require('deep-equal');

describe('ErrorResponse', function() {
    describe("Constructor", function()
    {
        it('Accept status code and details', function(){
            let response = new ErrorResponse("hi", 200, "world");
            assert.isTrue(equal({'statusCode':200, 'message': 'hi', 'details': 'world'}, response.body));

            //Verify error property
            assert.isUndefined(response.body['error']);
            assert.isDefined(response['error']);
            assert.equal('string', typeof response['error']);
            assert.equal('hi', response['error']);
        });
        it('Accept string', function(){
            let response = new ErrorResponse("hi");
            assert.isTrue(equal({'statusCode':500, 'message': 'hi', 'details': ''}, response.body));

            //Verify error property
            assert.isUndefined(response.body['error']);
            assert.isDefined(response['error']);
            assert.equal('string', typeof response['error']);
            assert.equal('hi', response['error']);
        });
        it('Accept ErrorResponse', function(){
            let response = new ErrorResponse(new ErrorResponse("hi", 500, "world"));
            assert.isTrue(equal({'statusCode':500, 'message': 'hi', 'details': 'world'}, response.body));

            //Verify error property
            assert.isUndefined(response.body['error']);
            assert.isDefined(response['error']);
            assert.isTrue(response['error'] instanceof ErrorResponse);
            assert.equal('hi', response['error'].message);
        });
        it('Accept Error', function(){
            let response = new ErrorResponse(new Error("hi"), 400);
            assert.equal(400, response.body.statusCode);
            assert.equal('hi', response.body.message);
            assert.isTrue(response.body.details.length > 0);

            //Verify error property
            assert.isUndefined(response.body['error']);
            assert.isDefined(response['error']);
            assert.isTrue(response['error'] instanceof Error);
            assert.equal('hi', response['error'].message);
            assert.isDefined(response['error'].stack);
        });
        it('isErrorResponse - false - plain object', function(){
            assert.isFalse(ErrorResponse.isErrorResponse({}))
        });
        it('isErrorResponse - true', function(){
            assert.isTrue(ErrorResponse.isErrorResponse(new ErrorResponse(new Error("hi"), 400)))
        });
        it('isErrorResponse - true - plain object', function(){
            assert.isTrue(ErrorResponse.isErrorResponse({statusCode:200, body:'' , message: '', details: '', error: '', extra: ''}));
        });
    });
});