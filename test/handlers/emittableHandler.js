const LogLevel = require('../../src/logLevel');
module.exports = function emittableHandler(event)
{
    this.emit(LogLevel.Trace, "Hello from emittableHandler");
};