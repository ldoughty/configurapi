const assert = require('chai').assert;
const Policy = require('../src/policy/policy');
const Result = require('../src/result');
const Event = require('../src/event');
const helper = require('./helper');

async function incrementAsync(count){
    return new Promise((resolve, reject) =>{setTimeout(()=>resolve(count+1), 1);});
}

describe('Policy', function() {
    describe("Process", function()
    {
        it('Synchornous handler', async function(){
            let policy = new Policy();
            policy.handler = function(ev)
            {
                ev.response.body = "here";
            };
            
            let ev = new Event(helper.createRequest('GET','/collections'));
            policy.process(ev);

            assert.equal('here', ev.response.body);
        });

        it('Asynchornous handler', async function(){
            let policy = new Policy();
            policy.handler = function(ev)
            {
                return new Promise(async (resolve, reject) =>{
                    ev.response.body = await incrementAsync(2);
                    resolve();
                });
            };
            
            let ev = new Event(helper.createRequest('GET','/collections'));
            await policy.process(ev);

            assert.equal('3', ev.response.body);
        });
    });

    describe("Context", function()
    {
        it('continue', async function(){
            let policy = new Policy();
            policy.handler = function(ev)
            {
                this.continue();
            };
            
            let ev = new Event(helper.createRequest('GET','/collections'));
            let result = await policy.process(ev);

            assert.equal(Result.Continue, result);
        });

        it('completed', async function(){
            let policy = new Policy();
            policy.handler = function(ev)
            {
                this.complete();
            };
            
            let ev = new Event(helper.createRequest('GET','/collections'));
            let result =  await policy.process(ev);

            assert.equal(Result.Completed, result);
        });

        it('catch', async function(){
            let policy = new Policy();
            policy.handler = function(ev)
            {
                this.catch('error_message');
            };
            
            let ev = new Event(helper.createRequest('GET','/collections'));

            let error;

            try
            {
                await policy.process(ev);
                
            }
            catch(e)
            {
                error = e;
            }

            assert.isDefined(error, 'Expect an exception to be thrown when catch() is used.');
            assert.equal('error_message', error)
        });
    });
});