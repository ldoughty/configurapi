const assert = require('chai').assert;
const PolicyHandlerLoader = require('../src/policy/policyHandlerLoader')


describe('PolicyHandler', function(){
    describe('load', function(){
        it('Invalid module', function(){
            assert.throw(() => new PolicyHandlerLoader().load('invalid-module'), 'Failed to load invalid-module');
        });
        it('Valid module', function(){
            assert.doesNotThrow(() => new PolicyHandlerLoader().load('http'));
        });
    });

    describe('loadCustomHandlers', function(){
        it('Invalid directory', function(){
            assert.throw(() => new PolicyHandlerLoader().loadCustomHandlers('invalid'), 'Could not load');
        });
        it('Valid module', function(){
            let loader = new PolicyHandlerLoader();
            assert.doesNotThrow(() => loader.loadCustomHandlers('./handlers'));
            assert.equal(8, loader.handlers.length);
        });
    });

    describe('get', function(){
        it('Nonexisting handler', function(){
            assert.throw(() => new PolicyHandlerLoader().get('nonexisting'), 'Handler \'nonexisting\' could not be found.');
        });
        it('Existing handler', function(){
            let loader = new PolicyHandlerLoader();
            loader.handlers.set("aHandler", function(){});
            assert.isFunction(loader.handlers.get("aHandler"));
        });
    });
});