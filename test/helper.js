let URL = require('url');

module.exports = {
    createRequest: function(method, path, payload = {}, headers = {}) {
        let url = URL.parse(path, true);
        return {
            method: method,
            url: url.pathname,
            query: url.query,
            params: {},
            payload: payload,
            path: url.pathname,
            headers: headers
        };
    },

    createResponse: function()
    {
        return mockHttp.createResponse();
    }
};