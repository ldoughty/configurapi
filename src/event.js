let Response = require("./response");
let uuid = require('uuid/v4');

let inflectors = require("inflection");

// Source: http://stackoverflow.com/questions/6491463/accessing-nested-javascript-objects-with-string-key
Object.byString = function(o, s) {
    s = s.replace(/\[(\w+)\]/g, '.$1'); // convert indexes to properties
    s = s.replace(/^\./, '');           // strip a leading dot
    var a = s.split('.');
    for (var i = 0, n = a.length; i < n; ++i) {
        var k = a[i];
        if (k in o) {
            o = o[k];
        } else {
            return;
        }
    }
    return o;
}


module.exports = class Event 
{
    constructor(request)
    {
        this.id = uuid();
        
        if('query' in request && 'correlationid' in request.query)
        {
            this.correlationId = request.query['correlationid'];
        }
        else if('headers' in request && 'correlation-id' in request.headers)
        {
            this.correlationId = request.headers['correlation-id'];
        }
        else
        {
            this.correlationId = this.id;
        }

        this.versionRegExp = new RegExp("^v\\d+(\\.\\d+)*$");
        this.params = {};
        this.name = request.name || "";
        this.request = request;
        this.response = new Response();
        
        this.identity = undefined;

        this.payload = this.request.payload;
        
    	this.method = request.method;
        
        let segments = request.path.split("/").filter(Boolean);

        if(segments.length === 0) return;

        //Extract version, if any.
        if(this.versionRegExp.test(segments[0]))
        {
            this.version = segments[0];
            segments = segments.slice(1);
        }

        //Extract resource-resourceId pair.
        for(let i=0, end=segments.length; i<end; i+=2)
        {
            let resource;
            let resourceId = '';

            if(i < end)
            {
                resource = segments[i];
            }

            if(i + 1 < end)
            {
                resourceId = segments[i+1];
            }

            if(resource.length === 0) break;
            
            let singularizedResource = inflectors.singularize(resource);

            //Change 'get' to 'list'
            if(!resourceId && this.method == 'get')
            {
                this.method = 'list';
            }
            else
            {
                resource = singularizedResource;
            }
            
            this.params[singularizedResource] = resourceId;
            this.name += `_${resource}`;
        }
        
        for(let key in request.query)
        {
            this.params[key] = request.query[key];
        }

        this.name = this.method + (this.version ? `_${this.version}`: "") + this.name;
    }

    resolve(obj)
    {
        if(typeof obj === 'string')
        {
            return this.resolveStr(obj);
        }
        else if(typeof obj === 'object')
        {
            for(let property in obj)
            {
                obj[property] = this.resolve(obj[property]);
            }
            return obj;
        }
        else
        {
            return obj;
        }
    }

    resolveStr(str)
    {
        let regex = /\$\([^\)]*\)/g;
        let matches = str.match(regex);
        
        if(!matches)
        {
            return str;
        }

        for(let match of matches)
        {
            let key = match.substr(2, match.length-3);
            let value = undefined;

            if(key.startsWith('$env.'))
            {
                //handle environment variables
                let variableName = key.substr(5);
                value = process.env[variableName];
            }
            else if(key.startsWith('$event.payload.') && this.payload)
            {
                //handle environment variables
                let path = key.substr(15);
                value = Object.byString(this.payload, path);
            }
            else if(key === '$event.payload' && this.payload)
            {
                value = this.payload;
            }
            else if(this.params[key] !== undefined)
            {
                value = this.params[key];
            }
            else if(key.indexOf('.') !== -1 || key.indexOf('[') !== -1) 
            {
                //handle resolving object using string path such as obj.prop.subprop.list[0]
                let index = key.indexOf('.');
                let objSection = key.substr(0, index);
                let propSection = key.substr(index+1);
                
                value = this.params[objSection];

                if(typeof value === 'object')
                {
                    value = Object.byString(value, propSection);
                }   
            }

            if(value === undefined)
            {
                value = match;
            }

            str = str.replace(match, value);
        }

        return str;
    }

    toString()
    {
        let ids = [];

        for(let resource in this.params)
        {
            ids.push(this.params[resource]);
        }

        return `${this.name}(${ids})`;
    }
};
