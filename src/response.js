module.exports = class Response
{
    constructor(body = "", statusCode = 200, headers = {})
    {
        this.statusCode = statusCode;
        this.body = body;
        this.headers = headers;
    }
};