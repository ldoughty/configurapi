const fs = require('fs');
const Dictionary = require('../dict');
const resolve = require('path').resolve;
const NestedError = require('nested-error-stacks');

module.exports = class PolicyHandlerLoader
{
    constructor()
    {
        this.handlers = new Dictionary();
    }

    get(handlerName)
    {
        if(this.handlers.has(handlerName))
        {
            return this.handlers.get(handlerName);
        }
        else
        {
            throw new Error(`Handler '${handlerName}' could not be found.`);
        }
    }

    load(moduleOrFileName)
    {
        let exports = this._resolve(moduleOrFileName);

        if(!exports)
        {
            throw new Error(`Cannot find '${moduleOrFileName}'`);
        }

        this.loadHandler(exports);
    }

    loadCustomHandlers(directory)
    {
        let absoultePath = resolve(process.cwd(), directory);

        try
        {   
            let paths = fs.readdirSync(absoultePath);
            
            for(let path of paths)
            {
                let filePath = `${absoultePath}/${path}`;
                let stat = fs.lstatSync(filePath);
                if(stat.isFile() && path.match(/\.[jt]s$/i) !== null)
                {
                    this.load(absoultePath + "/" + path.replace(/\.[jt]s$/i, ''));
                }
            }
        } catch(error)
        {
            throw new NestedError(`Could not load '${absoultePath}'`, error);
        }
    }

    _resolve(path) 
    {
        try 
        { 
            return require(path);
        }
        catch(e)
        {
            throw new NestedError(`Failed to load ${path}`, e);
        }

        return undefined;
    }

    loadHandler(obj) 
    {
        if(typeof obj === "function")
        {
            this.handlers.set(obj.name, obj);
        }
        else
        {
            for(let key in obj)
            {
                if(this._isHandler(obj, key))
                {
                    this.handlers.set(key, obj[key]);
                }
            }
        }
    }

    _isHandler(obj, key)
    {
        return (typeof obj[key] === "function") && key.match(/handler$/i) !== null;
    }
};