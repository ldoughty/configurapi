const Response = require('./response');

module.exports = class ErrorResponse extends Response
{

    constructor(message, statusCode = 500, details = '')
    {
        super('', statusCode, {'Content-Type': 'application/json'});

        this.error = message;
        this.message = message;
        this.details = details;

        if(message instanceof ErrorResponse)
        {
            let e = message;
            this.message = e.message;
            this.details = e.details;
            this.statusCode = e.statusCode;
        }
        else if(message instanceof Error)
        {
            this.message = message.message;
            this.details = message.stack;
        }
        else
        {
            this.message = message;
            this.details = details;
        }

        this.body = {'statusCode': this.statusCode, 'message': this.message, 'details': this.details};
    }

    static isErrorResponse(obj)
    {
        return typeof obj === 'object' && 'statusCode' in obj && 'error' in obj && 'message' in obj && 'details' in obj && 'body' in obj; 
    }
};
