module.exports = class Dictionary
{
    constructor()
    {
        this.items = {};
    }

    has(k)
    {
        return k in this.items;
    }

    get(k)
    {
        return this.items[k];
    }

    set(k, v)
    {
        this.items[k] = v;
    }

    clear()
    {
        this.items = {};
    }

    get length()
    {
        return Object.keys(this.items).length;
    }

    map(f)
    {
        for(let k of Object.keys(this.items))
        {
            f(this.items[k]);
        }
    }
};